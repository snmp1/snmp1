#|
SNMP1 - Simple Network Management Protocol for Common Lisp
Copyright (C) 2007  Johan Ur Riise

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
|#

(in-package "SNMP1")

(defparameter *example-decoded-response* 
  '(:SEQUENCE (:INTEGER 0) (:OCTET-STRING "public")
    (:RESPONSE (:INTEGER 12345) (:INTEGER 0) (:INTEGER 0)
     (:SEQUENCE
      (:SEQUENCE (:OBJECT-IDENTIFIER ".1.3.6.1.2.1.1.2.0")
		 (:OBJECT-IDENTIFIER ".1.3.6.1.4.1.8072.3.2.10")))))
  "Used in tests")


(defun make-buffer ()
    (make-array 300 :element-type '(unsigned-byte 8):fill-pointer 0))

(defgeneric == (a b)
  (:method ((a vector) (b vector))
    (and (= (length a) (length b)) (every #'= a b))))

(defun test-01 ()
  (format t "obs long value~%")
  (let ((buffer (make-buffer))
	(data (make-buffer)))
    (loop repeat 7559605 do (vector-push-extend 65 data))
    (ber-encode (list :octet-string data) buffer)
    #|(encode-length 7559605 buffer)|#
    (and (== (subseq buffer 1 5) #(#b10000011 #b01110011 #b01011001 #b10110101))
	 (= (+ 5 7559605) (length buffer)))))

(defun test-02 ()
  (let ((buffer (make-buffer))
	(data (make-buffer)))
    (loop repeat 102 do (vector-push-extend 65 data))
    (ber-encode `(:octet-string ,data) buffer)
    (eql (aref buffer 1) #b01100110)))

(defun test-03 ()
  (let ((buffer (make-buffer)))
    (ber-encode-integer-value -26955 buffer)
    (display buffer)
    (== buffer #( #b10010110 #b10110101))))

(defun test-03a ()
  (let ((buffer (make-buffer)))
    (ber-encode-integer-value 2147483648 buffer)
    (== buffer #(0 #b10000000 0 0 0))))

(defun test-04 ()
  (and (= -1 (ber-decode-integer-value #(255) 0 1))
       (= -26955 (ber-decode-integer-value #(#b10010110 #b10110101) 0 2))))

(defun test-06 ()
    
    (let ((buffer-in #(48 124 2 1 0 4 6 112 117 98 108 105 99 162 111 2 2 48 57 2 1 0 2 1 0 48 99 48
		       97 6 8 43 6 1 2 1 1 1 0 4 85 76 105 110 117 120 32 98 114 101 97 100 32 50 46
		       54 46 49 53 45 50 55 45 97 109 100 54 52 45 103 101 110 101 114 105 99 32 35
		       49 32 83 77 80 32 80 82 69 69 77 80 84 32 70 114 105 32 68 101 99 32 56 32 49
		       55 58 53 48 58 53 52 32 85 84 67 32 50 48 48 54 32 120 56 54 95 54 52))
	  (buffer (make-buffer)))
      (loop for x across buffer-in do (vector-push-extend x buffer))
      (ber-decode buffer 0 (length buffer))))

(defun test-07 ()
  (let ((buffer #(43 6 1 2 1 1 1 0)))
    (== (ber-decode-object-identifier-value buffer 0 (length buffer))
	#(1 3 6 1 2 1 1 1 0)))
  )

(defun test-08 ()
  (== (ber-encode '(:object-identifier #(1 3 6 3255)))
      #(#b00000110 4 #b00101011 #b00000110 #b10011001 #b00110111)))

(defun test-09 ()
  (let ((buffer #( #b00101011 #b00000110 #b10011001 #b00110111)))
    (== (ber-decode-object-identifier-value buffer 0 4)
	#(1 3 6 3255))))



(defun test-12 ()
  (let ((buffer #(5 0)))
    (tree-equal '(:null) (ber-decode buffer 0 (length buffer)))))

(defun test-13 ()
  (== (ber-encode '(:null nil)) #(5 0)))

  
(defun test-14 ()
  (== #(4 3 97 98 99) (ber-encode '(:octet-string "abc"))))

(defun test-15 ()
  (let ((buffer-in #(4 3 97 98 99))
	(buffer (make-buffer)))
    (loop for x across buffer-in do (vector-push-extend x buffer))
    (equal "abc" (second (ber-decode buffer 0 (length buffer))))))


(defun test-16 ()
  (udp-send-and-receive 
   #(127 0 0 1)
   161
   1
   3
   (ber-encode `(:sequence (:integer 0) ; version 1
			   (:octet-string "public")
			   (:get (:integer 12345)
				 (:integer 0)
				 (:integer 0)
				 (:sequence 
				  (:sequence (:object-identifier #(1 3 6 1 2 1 1 1 0)) (:null))))))))


(defun test-17 ()
  (let* ((to-send (ber-encode `(:sequence (:integer 0) ; version 1
					  (:octet-string "public")
					  (:getnext (:integer 12345)
						    (:integer 0)
						    (:integer 0)
						    (:sequence
						     (:sequence (:object-identifier #(1 3 6 1 2 1 1 1 0)) (:null))))))) 
	 (buffer (udp-send-and-receive 
		 #(127 0 0 1)
		 161
		 1
		 3
		 to-send)))
    (display to-send)
    (ber-decode buffer 0 (length buffer))))

(defun test-17a ()
  ;; using sequence for varbind, using one element list for null
  (let* ((to-send (ber-encode `(:sequence (:integer 0) ; version 1
					  (:octet-string "public")
					  (:getnext (:integer 12345)
						    (:integer 0)
						    (:integer 0)
						    (:sequence 
						     (:sequence (:object-identifier #(1 3 6 1 2 1 1 1 0)) (:null)))))))
	 (buffer (udp-send-and-receive 
		  #(127 0 0 1)
		  161
		  1
		  3
		  to-send)))
    (display to-send)
    (ber-decode buffer 0 (length buffer))))

(defun test-problem1 () 
  ;; empty varbind
  (let* ((a-buffer #(48 25 2 1 0 4 6 112 117 98 108 105 99 162 12 2 2 2 42 2 1 0 2 1 0 48 0))
	(buffer (make-array 300 :element-type '(unsigned-byte 8):fill-pointer 0)))
    (loop for x across a-buffer do (vector-push-extend x buffer))
    (display buffer (length buffer))
    (ber-decode buffer 0 (length buffer))))

(defun test-problem2 ()
  ;; octet string is not string
  (let* ((a-buffer #(48 51 2 1 0 4 6 112 117 98 108 105 99 162 38 2 2 1 94 2 1 0 2 1 0 48 26 48 24
               6 14 42 134 72 206 22 130 44 43 1 1 1 1 2 65 4 6 0 4 122 6 158 96))
         (buffer (make-array 300 :element-type '(unsigned-byte 8):fill-pointer 0)))
    (loop for x across a-buffer do (vector-push-extend x buffer))
    (display (subseq buffer 29 53))
    (ber-decode buffer 0 (length buffer))))

;;(defun test-snmpgetnext ()
;;  (snmpgetnext #(127 0 0 1) "public" #(0 0)))

;;(defun test-snmpgetnext2 ()
;;  (snmpgetnext2 #(127 0 0 1) "public" #(0 0)))

;;(defun test-snmpwalk ()
;;  (snmpwalk #(127 0 0 1) "public"))

;;(defun test-snmpget ()
;;  (let ((r1 (snmpget #(127 0 0 1) "public" ".1.3.6.1.2.1.1.2.0"))
;;	(r2 (snmpget #(127 0 0 1) "public" ".1.3.6.1.2.1.1.5.0")))
;;    (display r1 r2)))


(defun expose-bit-7 (octets)
  (map 'vector #'(lambda (octet)
		   (cons (ldb (byte 1 7) octet)(ldb (byte 7 0) octet)))
       octets))


(defun test-30-pdu-from-message ()
  (tree-equal (pdu-from-message *example-decoded-response*) 
	      '(:RESPONSE (:INTEGER 12345) (:INTEGER 0) (:INTEGER 0)
		(:SEQUENCE
		 (:SEQUENCE (:OBJECT-IDENTIFIER ".1.3.6.1.2.1.1.2.0")
		  (:OBJECT-IDENTIFIER ".1.3.6.1.4.1.8072.3.2.10"))))
	       :test #'equal))

(defun test-31-value-from-encoding ()
  (and (eql 9 (value-from-encoding '(:integer 9)))
       (equal ".1.3.6.1.4.1.8072.3.2.10" 
	      (value-from-encoding '(:OBJECT-IDENTIFIER ".1.3.6.1.4.1.8072.3.2.10")))))


(defun test-32-request-id ()
  (let ((copied-tree (copy-tree *example-decoded-response*))
	(expected '(:SEQUENCE (:INTEGER 0) (:OCTET-STRING "public")
		    (:RESPONSE (:INTEGER 888) (:INTEGER 0) (:INTEGER 0)
		     (:SEQUENCE
		      (:SEQUENCE (:OBJECT-IDENTIFIER ".1.3.6.1.2.1.1.2.0")
				 (:OBJECT-IDENTIFIER ".1.3.6.1.4.1.8072.3.2.10")))))))
   (and (eql 12345 (request-id *example-decoded-response*))
	(tree-equal expected (nreplace-request-id 888 copied-tree) :test #'equal))))

(defun test-40-oid-conversions ()
  (and (equalp #(1 3 5 6 7 333 233) (oid-string-to-oid ".1.3.5.6.7.333.233"))
       (equal  ".1.3.5.6.7.333.233" (oid-to-oid-string #(1 3 5 6 7 333 233)))
       (equalp #(2 3 4) (oid-basic-form ".2.3.4"))
       (equalp #(2 3 4) (oid-basic-form #(2 3 4)))
       (equalp #(1 3 6 1 2 1 1 2) (oid-basic-form ".iso.org.dod.internet.mgmt.mib-2.system.sysObjectID"))
       (equalp #(1 3 6 1 2 1 1 2) (oid-basic-form "sysObjectID"))
       (equalp #(1 3 6 1 2 1 1 2 0) (oid-basic-form ".iso.org.dod.internet.mgmt.mib-2.system.sysObjectID.0"))
       (equalp #(1 3 6 1 2 1 1 2 0) (oid-basic-form "sysObjectID.0"))
       (equal ".iso.org.dod.internet.mgmt.mib-2.system.sysObjectID" 
	       (symbolic-oid-from-oid #(1 3 6 1 2 1 1 2)))
       (equal ".iso.org.dod.internet.mgmt.mib-2.system.sysObjectID.0" 
	       (symbolic-oid-from-oid #(1 3 6 1 2 1 1 2 0)))
       (equal ".iso.org.dod.internet.mgmt.mib-2.system.77.0" 
	       (symbolic-oid-from-oid #(1 3 6 1 2 1 1 77 0)))
       (equal ".77.6.1.2.1.1.2.0" (symbolic-oid-from-oid #(2 77 6 1 2 1 1 2 0)))
       (equal ".iso.2.3.4.5" (symbolic-oid-from-oid #(1 2 3 4 5)))))






;; (defun test-60-snmpget ()
;;   (and )
;;   (snmp-get ".1.3.6.1.2.1.1.2.0"))



(defun test-33-varbind-list ()
  (let ((pdu (pdu-from-message *example-decoded-response*))
	(expected '(:SEQUENCE
		    (:SEQUENCE (:OBJECT-IDENTIFIER ".1.3.6.1.2.1.1.2.0")
		     (:OBJECT-IDENTIFIER ".1.3.6.1.4.1.8072.3.2.10")))))
    (and (tree-equal expected (varbind-list% pdu) :test #'equal)
	 (tree-equal expected (varbind-list *example-decoded-response*) :test #'equal))))


(defun test-100-mib-grep ()
  (equal ".iso.org.dod.internet.mgmt.mib-2.ianaifType"
	 (mib-grep "ianaifType")))

(defun test-101-mib-grep-hashed ()
  (let ((expected '(".iso.org.dod.internet.mgmt.mib-2.ianaifType"
		    ".iso.org.dod.internet.mgmt.mib-2.interfaces.ifTable.ifEntry.ifType")))
    (and (equal expected (mib-grep-hashed "ifType"))
       (equal expected (mib-grep-hashed "ifType")))))

(defun test-102-scalar ()
  (and (equalp #(1 2 3 4 5 6 0) (scalar #(1 2 3 4 5 6)))
       (equalp #(1 2 3 4 5 6 77) (scalar #(1 2 3 4 5 6) 77))))


(defun test-103-subidentifiers ()
  (and (equalp #("a" "b" "c") (subidentifiers ".a.b.c"))
       (equalp #("a" "b" "c") (subidentifiers "a.b.c"))
       (equalp #("a" 5 "c" 7) (subidentifiers ".a.5.c.7"))
       (equalp #("a" 5 "c" 7) (subidentifiers "a.5.c.7"))))

(defun test-104-triple ()
  (and
   (equalp '(#(1 2 3) :integer 66)
	   (varbind-to-triple '(:sequence ( :object-identifier #(1 2 3)) (:integer 66))))
   (equalp '(#(3 4 5) :octet-string #(6 7 8)) 
	   (varbind-to-triple '(:sequence (:object-identifier #(3 4 5)) (:octet-string #(6 7 8)))))
   (equalp '(#(3 4 5) :null nil ) 
	   (varbind-to-triple '(:sequence (:object-identifier #(3 4 5)) (:null))))
   (equalp '(#(3 4 5) nil nil ) 
	   (varbind-to-triple '(:sequence (:object-identifier #(3 4 5)))))
   (equalp '(:sequence (:object-identifier #(3 4 5)) (:octet-string #(6 7 8))) 
	   (triple-to-varbind '(#(3 4 5) :octet-string #(6 7 8))))
   (equalp '(:sequence (:object-identifier #(3 4 5)) (:null)) 
	   (triple-to-varbind '(#(3 4 5) :null nil )))))


(defun test-250-snmp-get-many- ()
  (let ((expected '((#(1 3 6 1 2 1 1 9 1 2 2) :object-identifier #(1 3 6 1 6 3 1))
		    (#(1 3 6 1 2 1 1 9 1 2 6) :object-identifier #(1 3 6 1 6 3 16 2 2 1)))))
    (equalp expected
	    (snmp-get-many- '(#(1 3 6 1 2 1 1 9 1 2 2)
			      #( 1 3 6 1 2 1 1 9 1 2 6))
		    12345))))

(defun test-350-snmp-get- ()
  (and (equalp '(#(1 3 6 1 2 1 1 9 1 2 3):object-identifier #(1 3 6 1 2 1 49))
	       (snmp-get- #(1 3 6 1 2 1 1 9 1 2 3)))
       (equalp '(#(1 3 6 1 2 1 1 9 1 2 6) :object-identifier #(1 3 6 1 6 3 16 2 2 1))
	      (snmp-get- #(1 3 6 1 2 1 1 9 1 2 6)))
       ))

(defun test-450-snmp-get ()
  (and (equalp '(".iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysOREntry.sysORID.2"
		 :OBJECT-IDENTIFIER 
		 ".iso.org.dod.internet.snmpV2.snmpModules.snmpMIB") 
	       (snmp-get 
		".iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysOREntry.sysORID.2"))
       (equalp '(".iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysOREntry.sysORID.6"
		 :OBJECT-IDENTIFIER
		 ".iso.org.dod.internet.snmpV2.snmpModules.snmpVacmMIB.vacmMIBConformance.vacmMIBGroups.vacmBasicGroup")
	      (snmp-get ".1.3.6.1.2.1.1.9.1.2.6"))))

(defun test-451-get-wrong-oid ()
    ;; shuould not crash randomly. The oid will be nil here
    (null (snmp-get "sysObjectOD.0")))



(defun compute-sort-keys (sym)
  (let ((name (symbol-name sym)))
    (multiple-value-bind (int eaten) (parse-integer (subseq name 5) :junk-allowed t)
      (let ((alfa (subseq name (+ 5 eaten))))
        (values int alfa)))))



(defun test-symbol-less (sym-a sym-b)
  (multiple-value-bind (int-a alf-a) (compute-sort-keys sym-a)
       (multiple-value-bind (int-b alf-b) (compute-sort-keys sym-b)
            (if (eql int-a int-b)
                (string< alf-a alf-b)
                (< int-a int-b)))))

(defun run-tests ()
  ;; All symbols in this package beginning with test and which is a function
  (let (test-funcs
        (totres t))
    (loop for s being each present-symbol  do
         (let ((res (search "TEST-" (symbol-name s))))
           (when (and res (= 0 res)
                      (parse-integer (symbol-name s) :start 5 :junk-allowed t)
                      (fboundp s))
             (push s test-funcs))))

    (setf test-funcs (sort test-funcs #'test-symbol-less))
    (loop for sym in test-funcs do
         (let ((res (funcall sym)))
           (format t "~a: ~a~%" sym (if res "PASSED" "FAILED"))
           (unless res (setf totres nil))))
    totres))

(define-symbol-macro tt (run-tests))

