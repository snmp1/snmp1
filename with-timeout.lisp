#|
SNMP1 - Simple Network Management Protocol for Common Lisp
Copyright (C) 2007  Johan Ur Riise

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
|#
(in-package "SNMP1")
(shadow 'with-timeout)
(defmacro with-timeout (duration &rest forms)
  (let ((timer (gensym "WITH-TIMEOUT-TIMER")))
    `(LET ((,timer (MAKE-TIMER (LAMBDA () (CERROR "Continue" 'TIMEOUT)))))
      (SCHEDULE-TIMER ,timer ,duration)
      (UNWIND-PROTECT
           (PROGN ,@forms )
        (UNSCHEDULE-TIMER ,timer))))
    )

