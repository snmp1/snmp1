;;;; Loop reading udp-packets coming in to port 162
;;;; resend packets to specified port on localhost


;; test:  nc -u localhost 2162
;;        nc -u -l 4162
;;        (trap-redirector (list 4162))

(defun trap-redirector (to-ports)
  "resends packets coming in on trap port to localhost on each to-port in the list"
  (let ((socket (make-instance 'sb-bsd-sockets:inet-socket :type :datagram :protocol :udp))
	(buffer (make-array 1500 :element-type '(unsigned-byte 8))))
    (unwind-protect 
	 (progn
	   (sb-bsd-sockets:socket-bind socket #(0 0 0 0) 162)
	   (loop do  
		(multiple-value-bind (buf len peer-addr) (sb-bsd-sockets:socket-receive socket buffer nil)
		  (declare (ignore buf peer-addr))
		  (loop for to-port in to-ports do 
		       (sb-bsd-sockets:socket-send socket buffer len :address (list #(127 0 0 1) to-port))))))
      (sb-bsd-sockets:socket-close socket))))

;;(trap-redirector (list 4162))
