;; -*- mode: Lisp; coding: utf-8; -*-

;; SNMP1 - Simple Network Management Protocol for Common Lisp

;; This software is Copyright (c) Johan Ur Riise 2007
;; Johan Ur Riise grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL.

;; (asdf:operate 'asdf:load-op :split-sequence)
;; (asdf:operate 'asdf:load-op :snmp1)
(require 'sb-bsd-sockets)
(defsystem :snmp1
    :name "snmp1"
    :author "Johan Ur Riise"
    :version "1.00"
    :serial t
    :components ((:file "package")
		 (:file "display")
		 (:file "ber")
		 (:file "mib")
		 (:file "snmp"))
    :depends-on ("split-sequence"))


