#|
SNMP1 - Simple Network Management Protocol for Common Lisp
Copyright (C) 2007  Johan Ur Riise

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
|#
(in-package :snmp1)

(defmacro display (&rest symbols)
  (let ((format-statements 
	  (loop 
	     for sym in symbols
	     collect `(format t  "~a: ~s~%" ',sym ,sym))))
    `(progn ,@format-statements)))


(defmacro with-printed-result (&rest forms)
  (let* ((result (gensym))
	 (printed-forms-in-list (write-to-string forms))
	 (printed-forms (subseq printed-forms-in-list
				1 (1- (length printed-forms-in-list))))
	 (form-coupé (subseq  printed-forms 
			      0 (min #1=30 (length printed-forms))))
	 (was-cut-off (when (> (length printed-forms) #1#) t)))
    `(let ((,result (progn ,@forms)))
       (format t "The value of ~a~a was: ~s~%"
	       ,form-coupé (if ,was-cut-off "..." ""),result)
       ,result)))


;; This is from Ro* War**** on c.l.l
(defmacro dbgvht ((&optional (where "Unknown Location")) &rest forms)
  `(progn
     (format t "~&<br>DBGV: At ~a<br>~%" ',where)
     ,@(loop for form in forms collect
	    `(format t "&nbsp;&nbsp;&nbsp;~a = ~s<br>~%" ',form ,form))))






