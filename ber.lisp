;; SNMP1 - Simple Network Management Protocol for Common Lisp

;; This software is Copyright (c) Johan Ur Riise 2007
;; Johan Ur Riise grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL.

(in-package "SNMP1")

(defun oid-string-to-oid (oid-string)
  "Convert string in form .1.3.5.6.7.333.233 to oid #(1 3 5 6 7 333 233)"
  (let ((result (make-array 0 :fill-pointer 0)))
    (loop for subidentifier in (split-sequence:split-sequence #\. oid-string :remove-empty-subseqs t)
       do (vector-push-extend (read-from-string subidentifier) result))
    result))

(defun oid-to-oid-string (oid)
  "Convert oid to string in form .1.3.5.6.7.333.233"
  (with-output-to-string (s)    
    (loop for sub-identifier across oid do
	 (write-char #\. s)
	 (prin1 sub-identifier s))))

(let ((tag->octet (make-hash-table))
	(octet->tag (make-hash-table))
	(tag-list (list
		   (list :integer           2)
		   (list :octet-string      4)
		   (list :null              5)
		   (list :object-identifier 6)
		   (list :ipaddress         64)
		   (list :counter           65)
		   (list :gauge             66)
		   (list :timeticks         67)
		   (list :opaque            68)
		   (list :sequence          #x30)
		   (list :get               #xa0)
		   (list :getnext           #xa1)
		   (list :response          #xa2)
		   (list :set               #xa3)
		   (list :trap              #xa4))))
    (loop for (id value) in tag-list do
	 (setf (gethash id tag->octet) value)
	 (setf (gethash value octet->tag) id))
    (defun encode-tag (tag) 
      "Convert a symbolic tag value to an octet"
      (gethash tag tag->octet))
    (defun decode-tag (octet) 
      "Convert a tag octest to its symbolic value"
      (gethash octet octet->tag))
    )
(defun container-type-p (tag)
  "Returns true if the tag is one of the container tags"
  (member tag (list :sequence :get :getnext :response :set :trap)))

(defun integer-type-p (tag)
  "Returns true if the tag is one of the ineger type tags"
  (member tag (list :integer :counter :gauge :timeticks)))

(defun octet-string-type-p (tag)
  "Returns true if tag is one of the tags that encode octet strings"
  (member tag (list :octet-string :opaque :ipaddress))) 

(defun object-identifier-type-p (tag)
  "Return true if the tag is :object-identitier"
  (member tag (list :object-identifier)))

(defun null-type-p (tag)
  "Returns true if the tag is :null"
  (member tag (list :null)))

(defun decode-length (from-array pos)
  "Return length and newpos and length-of-length as values"
  (let ((numbytes (aref from-array pos)))
    (if (not (logbitp 7 numbytes)) 
	(values numbytes (1+ pos) 1)
	(let ((length-of-length (ldb (byte 7 0) numbytes))
	      (real-length 0))
	  (loop 
	     for length-octet from (1- length-of-length) downto 0
	     for ref from (+ pos 1)
	     do (setf (ldb (byte 8 (* 8 length-octet)) real-length) (aref from-array ref)))
	  (values real-length (+ pos 1 length-of-length) (+ 1 length-of-length))))))

(defun ber-decode-integer-value (buffer start end) 
  "Return the integer-value starting"
  (let (result)
    (if (logbitp 7 (aref buffer start))
	(setf result -1)
	(setf result 0))
    (loop 
       for buffer-pos downfrom (1- end) to start
       for byte-pos from 0 by 8
       do (setf (ldb (byte 8 byte-pos) result)
		(aref buffer  buffer-pos)))
    result))

;; The object identifier encoding
;; 
;; An object is an array of integers, but can also be expressed in a string
;; where each integer is separated by dots. For instance
;;  
;; SNMP1> (oid-string-to-oid ".1.3.6.1.4.1.8072.3.2.10")
;; #(1 3 6 1 4 1 8072 3 2 10)
;; SNMP1> 
;;
;; Note, this array is not the octet encoding, rather the most 
;; basic representation of the oid in this package.
;;
;; Encoding of the object identifier is mostly one byte per sub-identifier,
;; with two exceptions. 
;;
;; The first exception is that the two first sub-identifiers are compressed
;; into one octet. So encoding of #(1 3 6) is octets #(43 6) (--ignoring tag and
;; length here) rather than octets #(1 3 6)
;;
;; SNMP1> (ber-encode '(:object-identifier #(1 3 6)))
;; #(6 2 43 6)
;; SNMP1> 
;;
;; The other exceptions is for sub-identifiers greater than 256, which is the
;; maximum unsigned number that an octet can hold. To solve this problem,
;; bit 7 of each octet is set when there are more octets for a sub-identifier.
;; This also means we have only seven bits left to encode the sub-identifier,
;; and the maximum sub-identifier that can be coded in one octet then
;; becomes  127.
;;
;; This can be seen by encoding the first oid above.
;; 
;; SNMP1> (ber-encode '(:object-identifier ".1.3.6.1.4.1.8072.3.2.10"))
;; #(6 10 43 6 1 4 1 191 8 3 2 10)
;; SNMP1> 
;;
;; Now ignoring tag octet, length octet and the two first sub-identifiers,
;; we can recognize 6 1 4 and 1 in the buffer. But the next one 8072, we
;; can not see. It is larger than 127 and therefore encoded in
;; more than one octet.
;;
;; We can split each octet in its seventh bit and the rest of the bits to
;; see the idea clearer.
;;
;; SNMP1> (map 'vector #'(lambda (octet)
;; 		   (cons (ldb (byte 1 7) octet)(ldb (byte 7 0) octet)))
;;       *)
;; #((0 . 6) (0 . 10) (0 . 43) (0 . 6) (0 . 1) (0 . 4) (0 . 1) (1 . 63) (0 . 8)
;;   (0 . 3) (0 . 2) (0 . 10))
;; SNMP1> 
;; 
;; Here we see that the number 191 really is the seven bit number 63 with
;; the octet's seventh bit set to signal that next octet, 8, is part of the same
;; subidentifier.
;;
;; we find the real subidentifier with this formula:
;;
;; SNMP1> (+ (* 63 128) 8)
;; 8072
;; SNMP1> 


 


(defun ber-decode-object-identifier-value (buffer start end)
  "Decodes part of the octet string array and returns an array of subidentifiers
Each subidentifier can be one or more octets. Each octet that has its seventh 
bit set, continues to the next octet. The first octet is special, as it
ecodes the two first subidentifiers."
  (let ((result (make-array 20 :adjustable t :fill-pointer 0)))
    ;; first and second oid are special
    
    (multiple-value-bind (first-sub-identifier second-sub-identifier) 
	(truncate (aref buffer start) 40)
      (vector-push-extend first-sub-identifier result)
      (vector-push-extend second-sub-identifier result))
    (let ((pos (1+ start))
	  octet
	  subidentifier)
     (loop 
       while (< pos end )
       do 
	 (setf subidentifier 0)
	 (loop 
	    do 
	      (setf octet (aref buffer pos))
	    do  

	      (setf subidentifier (+ (* subidentifier 128) (ldb (byte 7 0) octet)))
	      (incf pos)
	    while (logbitp 7 octet))
	 (vector-push-extend subidentifier result)
	 ))
    result))

(defun ber-encode-integer-value (value to-array)
  "Push value as integer to the tail oof the array. Every bit of
each octet is used, but we must make sure that the first octet
is less than 128 for positive numbers, as the 7.th bit in the
first octet signals a negative number"
  (let ((numbytes (ceiling (1+ (integer-length value)) 8)))
    (loop for pos from (1- numbytes)  downto 0  
       do (vector-push-extend (ldb (byte 8 (* pos 8)) value) to-array))
    to-array))


(defun ber-encode-octet-string-value (value to-array)
  "Push  value as octet-string. If the value is a string,
the result mitht be longer, depending on the current external
format"
  (when (typep value 'string) (setf value (string-to-octets value)))
  (loop for octet across value do (vector-push-extend octet to-array))
  to-array)

(defun ber-encode-object-identifier-value (in-value to-array)
  "Push value as octet-string Return to-array. The first two 
subidentifers go into one octet. Most other subidentifiers go into
one octet each. If a subidentifier is greater than 127, several 
octets are usec"
  (let ((value (if (stringp in-value) (oid-string-to-oid in-value) in-value)))
    ;; first and second subidentifier compressed into one octet
    (vector-push-extend (+ (* 40 (aref value 0)) (aref value 1)) to-array)
    
    (loop for sub-identifier across (subseq value 2) do
	 
	 (if (zerop sub-identifier)
	     (vector-push-extend 0 to-array)
	     (loop for chunk from (1- (ceiling (integer-length sub-identifier) 7)) downto 0 do
		  (let ((out-octet (ldb (byte 7 (* chunk 7)) sub-identifier)))
		    (unless (zerop chunk) (setf out-octet (logior #b10000000 out-octet)))
		    (vector-push-extend out-octet to-array)))))
    to-array))



(defun ber-encode (what &optional buffer)
  "Encode a single tag and value, or recursively encode a sequence.
Example of input is '(:sequence (:object-identifier #(1 3 4 5 6)) (:integer 42))
Normally, this function is called on the complete snmp message, which is such
sequence"
  (unless buffer (setf buffer (make-array 50 :element-type '(unsigned-byte 8) :fill-pointer 0)))
  (let ((tag (first what))
	(encoded-tag (encode-tag (first what)))
	(start-pos (fill-pointer buffer)))
    (vector-push-extend  encoded-tag buffer)
    ;; unfortunately we don't know length of length at this time. 
    ;; Guess length of length is 1, reserve an octet by pushing a zero to the buffer.
    (vector-push-extend 0 buffer)
    (cond
	((container-type-p tag)
	 (loop for thing in (cdr what) do (ber-encode thing buffer)))
	((octet-string-type-p tag) (ber-encode-octet-string-value (second what) buffer))
	((integer-type-p tag) (ber-encode-integer-value (second what) buffer))
	((object-identifier-type-p tag) (ber-encode-object-identifier-value (second what) buffer))
	((null-type-p tag) #|already done|#))
    (let ((length-of-value (- (fill-pointer buffer) (+ start-pos 2))))
	;; if length is 127 or less, we can place it directly in the reserved octet
	(if (< length-of-value 128)
	    (setf (aref buffer (+ start-pos 1)) length-of-value)
	    ;; our guess was wrong, now we have to move the value some places to the right
	    (let ((length-of-length (ceiling (1+ (integer-length length-of-value)) 8)))
	      (setf (aref buffer (+ start-pos 1)) (logior #b10000000 (ldb (byte 8 0) length-of-length )))
	      (adjust-array buffer #1=(+ length-of-length (fill-pointer buffer)) :fill-pointer #1#)
	      (loop 
		 for to-pos from (1- (fill-pointer buffer)) downto (+ start-pos length-of-length 2)
		 for from-pos = (- (fill-pointer buffer) length-of-length 1) then  (incf from-pos -1)
		   do (setf (aref buffer to-pos) (aref buffer from-pos)))
	      ;; now we can output the real length
	      (loop for pos from (1- length-of-length)  downto 0
		   for out-pos from (+ start-pos 2)
		 do (setf (aref buffer out-pos) (ldb (byte 8 (* pos 8)) length-of-value)))
	      
	      ))))
  buffer)

(defun ber-decode (buffer &optional (input-start 0) input-end (level 0))
  "the buffer is an octet string vector received from the net. It normally start with
a sequence containing the rest of the message. When a sequence tag is found, the 
function calls itself recursively to decode the contents.
Resurn the buffer in list form and length of buffer that is used as second value"
  (when (null buffer) (return-from ber-decode))
  (unless input-end (setf input-end (length buffer)))
  (let* ((start input-start)  
	 start-of-length
	 start-value
	 tag
	 length
	 end-value 
	 result)
    (loop
       do
	 (setf tag (decode-tag (aref buffer start)))
	 (setf start-of-length (1+ start))
	 (multiple-value-setq (length start-value) (decode-length buffer start-of-length))
	 (setf end-value (+ start-value length))
	 (cond
	   ((= start-value end-value) ; covers :null and empty sequences
	    (push (list tag) result))
	   ((container-type-p tag) 
	    (let (container)
	      (push tag container)
	      (loop for sub-element in (ber-decode buffer start-value end-value (1+ level))
		 do (push sub-element container))
	      (push (reverse container) result)))
	   ((integer-type-p tag) (push (list tag (ber-decode-integer-value buffer start-value end-value)) result))
	   ((octet-string-type-p tag) 
	    (push (list tag (subseq buffer start-value end-value)) result))
	   ((object-identifier-type-p tag) 
	    (push (list tag (ber-decode-object-identifier-value buffer start-value end-value)) result))
	   )
	 (setf start end-value)
       while (< start input-end))
    (setf result (reverse result))
    (if (and (= 0 level) (= 1 (length result)))
      (values (first result) end-value)
      (values result end-value))))

	
