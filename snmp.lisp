;; -*- mode: Lisp; coding: utf-8; -*-

;; SNMP1 - Simple Network Management Protocol for Common Lisp

;; This software is Copyright (c) Johan Ur Riise 2007
;; Johan Ur Riise grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL.

(in-package "SNMP1")

(defparameter *community* "public"
  "The default community string used. Assign your own value to this dynamic
variable before you call any snmp functions")
(defparameter *agent-ip* #(127 0 0 1)
  "The default ip address of the snmp agent you want to communicate with.
Assign your own value to this before you call any of the snmp functions.
The ip-address can be in any form; dotted quad, integer array or a single
number")
(defparameter *agent-port* 161
  "The default udp port where the agent listens for incoming calls. Change this 
if the snmp-agent you try to communicate with listens on another port. Normally
the agent listens on the default port")
(defparameter *wait* 1
  "This is the number of seconds we wait for an snmp agent to answer before
we try again or give up. The time can also be specified with a float.")
(defparameter *retries* 3
  "The maximum number of times we retry a communication with the agent")

(defun ip-string-to-ip-octets (dotted-quad)
 "Conversion of ip, example 127.0.0.1 as string to #(127 0 0 1). There
is also a from any form to ip octets conversion function"
  (let ((list (split-sequence:split-sequence  #\. dotted-quad))
        (vector (make-array 4)))
    (loop for n from 0 for component in list do (setf (aref vector n) (parse-integer component)))
    vector))

(defun ip-string-to-numeric (dotted-quad)
 "Convert for example 80.68.86.115 as string to a single integer 1346655859. Did
you know that you can paste this integer directly into a web browser?"
  (let ((octets (ip-string-to-ip-octets dotted-quad))
        (ip-numeric 0))
    (loop for octet across octets do
          (setf ip-numeric (+ (* ip-numeric 256) octet)))
    ip-numeric))

(defun ip-numeric-to-ip-octets (ip-numeric)
 "Convert an ip address expressed as a single intger, to its
octet array form"
  (apply #'vector (reverse (loop for x from 1 to 4 
                                 collect (ldb (byte 8 0) ip-numeric)
                                 do (setf ip-numeric (truncate ip-numeric 256))))))

(defun ip-octets-to-ip-string (ip-octets)
 "Convert an ip adress, example #(127 0 0 1) to its strign form 127.0.0.1"
  (format nil "~{~d.~d.~d.~d~}" (loop for o across ip-octets collect o)))

(defun ip-numeric (ip-some-form)
 "Convert an ip adress in any of the three forms to a single integer"
  (typecase ip-some-form
    (simple-vector (ip-string-to-numeric (ip-octets-to-ip-string ip-some-form)))
    (string (ip-string-to-numeric ip-some-form))
    (otherwise ip-some-form)))

(defun ip-octets (ip-some-form)
  "Convert an ip adress in any of the three forms to an array of four integers"
  (typecase ip-some-form
    (integer (ip-numeric-to-ip-octets ip-some-form))
    (string (ip-string-to-ip-octets ip-some-form))
    (otherwise ip-some-form)))

(defun ip-string (ip-some-form)
  "Convert an ip adress in any of the three forms to the dotted quad string form"
  (typecase ip-some-form
    (simple-vector (ip-octets-to-ip-string ip-some-form))
    (integer (ip-octets-to-ip-string (ip-numeric-to-ip-octets ip-some-form)))
    (otherwise ip-some-form)))


;; (defun oid-less (a-in b-in)
;;   (cond ((null a-in) nil)
;;         ((null b-in) t)
;;         (t (loop for a-sub across (oid-string-to-oid a-in)
;;                  for b-sub across (oid-string-to-oid b-in)
;;                  when (not (= a-sub b-sub)) do (return-from oid-less (< a-sub b-sub)))))
;;   )



(defun pdu-from-message (decoded-message)
 "Extract the Protocol Data Unit from a decoded message"
  (fourth decoded-message))

(defun value-from-encoding (encoding)
  "Extract the value from a single encoding, example (:integer 5) produces 5"
  (second encoding))

(defun request-id (decoded-message)
  "Extract the request identifier from a message. We can validate the integrity
of a response by checking that the recieved request-id is the same we used
in he corresponding get/set"
  (value-from-encoding (second (pdu-from-message decoded-message))))

;; (defun nreplace-request-id (new-value decoded-message) 
;; ;;(888 copied-tree)
;;   (let ((interesting-cons (last (second (pdu-from-message decoded-message)))))
;;     (rplaca interesting-cons new-value)
;;     decoded-message)
;; )

(defun varbind-list% (decoded-pdu)
 "Return the sequence containing all the variable bindings. Note that the input
here is the pdu part of the message, not the whole message"
  (fifth decoded-pdu))

(defun varbind-list (message)
 "Return the sequence containing all the variable bindings from a message"
  (varbind-list% (pdu-from-message message)))

;; (defun oid-and-value (varbind)
;;   (let ((oid-encoding (second varbind))
;;         (value-encoding (third varbind)))
;;     (list (value-from-encoding oid-encoding) (value-from-encoding value-encoding))))

(defun compose-varbind-list (oids)
  "Create a varbind-list suitable for ber-encode from a list of oids
ignore eny null oids"
  (let ((vars (loop for oid in (remove nil oids) collect `(:sequence (:object-identifier ,oid) (:null)))))
    (push :sequence vars)))

(defun varbind-to-triple (varbind)
 "Reduce a varbind sequence to a list of oid, type and value"
  (let ((requested-oid (second (second varbind)))
	(tag (first (third varbind)))
	(value (second (third varbind))))
    (list requested-oid tag value)))

(defun triples-from-decoded-message (decoded-message)
 "Return the varbinds in a message as a list of oid, type and value triples"
  (let ((varbind-list (varbind-list decoded-message)))
    (loop for pair in (cdr varbind-list) collect  (varbind-to-triple pair))))

;; (defun oids-and-values-from-message (message)
;;   (let ((varbind-list (varbind-list message)))
;;     ;;(mapcar #'oid-and-value varbinds)
;;     (loop for pair in (cdr varbind-list) collect  (oid-and-value pair) )
;;     ))

(defun udp-send-and-receive (host port timeout repetitions message)
  "send one pqcket and receive one packet. Do timeouts and retries.
This function is specific to sbcl"
  (handler-case 
      (let ((socket (make-instance 'sb-bsd-sockets:inet-socket :protocol :udp :type :datagram))
            result
            (recvbuf (make-array 2000 :element-type '(unsigned-byte 8))))
        (loop
         repeat repetitions
         do 
	 (sb-bsd-sockets:socket-send socket message nil :address (list host port))
	 (setf result 
	       (handler-case
		   (with-timeout timeout 
		     (multiple-value-bind (buf len peer-addr) 
			 (sb-bsd-sockets:socket-receive socket recvbuf 2000)
		       (declare (ignore peer-addr))
		       (subseq buf 0 len)))
		 (timeout ()  #|(display :hei )|# nil)))
         until result)
        (sb-BSD-SOCKETS:SOCKET-CLOSE socket)
        result)
    (sb-bsd-sockets:socket-error ())))


(defun snmp-get-many- (oids &optional (request-id (random 1000)))
  "Constructs the get pdu, inserts a random request-id if none is
spplied, checks the request-id, decodes the answer"
  (let* ((*agent-ip* (if (stringp *agent-ip* )(ip-string-to-ip-octets *agent-ip*) *agent-ip*))
	 (varbind-list (compose-varbind-list oids))
	 (un-encoded-message `(:sequence (:integer 0) ; version 1
					 (:octet-string ,*community*)
					 (:get (:integer ,request-id)
					       (:integer 0)
					       (:integer 0)
					       ,varbind-list)))
	 
	 (response-buffer (udp-send-and-receive 
			   *agent-ip*
			   *agent-port*
			   *wait*
			   *retries*
			   (ber-encode un-encoded-message)))
	 (decoded-message (ber-decode response-buffer 0 (length response-buffer))))
    ;;(print un-encoded-message netelements::*stdout*)
    (when (eql request-id (request-id decoded-message))
      (triples-from-decoded-message decoded-message))))
(defun oid-basic-form (oid)
 "Convert an oid in diverse symbolic forms, string or already basic form
to the basic form, which is an array"
  (cond 
    ;; ".2.3.4.5.4.5"
    ((and (stringp oid) (every #'(lambda (char) (or (digit-char-p char) (char= #\. char))) oid))
	 (oid-string-to-oid oid))
    ;; "sysObjectID"
    ((and (stringp oid) (not (position #\. oid)))
     (oid-from-trailing-subidentifier oid))
    ;; "sysObjectID.0"
    ((and (stringp oid) (= (count #\. oid) 1))
     (let ((point-pos (position #\. oid)))
       (let* ((symbolic-part (subseq oid 0 point-pos))
	      (trailing-digits (subseq oid (1+ point-pos)))
	      (symbolic-part-oid (oid-from-trailing-subidentifier symbolic-part)))
	 ;; if tests dont succed, resturn nil
	 (when (and symbolic-part-oid (every #'digit-char-p trailing-digits)) 
	   (scalar symbolic-part-oid (parse-integer trailing-digits))))))
    ((stringp oid)
     (let* ((last-dot (position #\. oid :from-end t))
	    (partial-oid (subseq oid 0 last-dot))
	    (trailing-digits (subseq oid (1+ last-dot))))
       (if (every #'digit-char-p trailing-digits)
	   ;;".iso.org.dod.internet.mgmt.mib-2.system.sysObjectID.0"
	   (let ((translated-part (oid-from-symbolic-oid partial-oid)))
	     ;; return 0 if oid not found in hash
	     (when translated-part
	       (scalar translated-part (parse-integer trailing-digits))))
	   ;;".iso.org.dod.internet.mgmt.mib-2.system.sysObjectID"
	   (oid-from-symbolic-oid oid))))
    ;;#(1 2 3)
    (t oid)))

(defun snmp-get- (oid)
 "Request a single value from the agent, but do not transform the
result. Resultt is a triple of object identifier, type and value"
  (let ((triple-list (snmp-get-many- (list (oid-basic-form oid)))))
    (first triple-list)))







;; (defun snmp-getnext (ip community oid)
;;   (let* ((seq (random 1000))
;; 	 (pdu `(:getnext (:integer ,seq)
;;                 (:integer 0)
;;                 (:integer 0)
;;                 (:sequence 
;; 		 (:sequence (:object-identifier ,oid) (:null)))))
;; 	 (req `(:sequence (:integer 0) ; version 1
;;                 (:octet-string ,community)
;;                 ,pdu))
;; 	 (request-buffer (ber-encode req))
;; 	 (response-buffer  (udp-send-and-receive 
;;                             ip
;;                             161
;;                             1
;;                             3
;;                             request-buffer)))
;;     ;;(display response-buffer)
;;     (let* ((response (ber-decode response-buffer 0 (length response-buffer)))
;;            (varbinds (fifth (fourth response)))
;;            (varbind (second varbinds)))
;;       ;;(display response)
;;       ;;(display varbinds)
;;       ;;(display varbind)
;;       (values (second varbind) (third varbind)))
    
    
;;     ))

;; (defun snmp-getnext2 (ip community oid)
;;   (let ((response-buffer (udp-send-and-receive 
;;                           ip
;;                           161
;;                           1
;;                           3
;;                           (ber-encode `(:sequence (:integer 0) ; version 1
;;                                         (:octet-string ,community)
;;                                         (:getnext (:integer 12345)
;;                                          (:integer 0)
;;                                          (:integer 0)
;;                                          (:sequence (:sequence (:object-identifier ,oid) (:null)))))))))
;;     (ber-decode response-buffer 0 (length response-buffer))))


;; (defun snmp-walk (ip community &optional (start-oid #(0 0)) )
;;   (let ((next-oid start-oid)
;; 	response-oid
;; 	value)
;;     (loop 
;;        while next-oid 
;;        do
;; 	 (multiple-value-setq (response-oid value) (snmp-getnext ip community next-oid))
;;        until (equal next-oid (second response-oid))
;;        do 
;; 	 (setf next-oid (second response-oid))
;; 	 (format t "~s ~s~%" response-oid value))))



;; (defun triple-to-varbind (triple)
;;   (if (third triple)
;;       `(:sequence (:object-identifier ,(first triple))
;; 	     (,(second triple) ,(third triple)))
;;       ;; f.ex (#(1 2 3 4 5) :null nil)
;;       `(:sequence (:object-identifier ,(first triple))
;; 	     (,(second triple)))))


(defun translate-triple (triple)
 "Translate object identifiers in the triple to its symbolic form,
translacte octet strings to strings, and enumerator integers to
symbolic form"
  (let ((translated-oid (symbolic-oid-from-oid (first triple)))
        (tag (second triple))
        (value (third triple)))
    (cond ((object-identifier-type-p tag)
           (list translated-oid tag (symbolic-oid-from-oid value)))
          ((octet-string-type-p tag)
           (let ((translated-value 
                  (handler-case (octets-to-string value)
                    (t () value))))
             (list translated-oid tag translated-value)))
          ((integer-type-p tag)
           (let ((maybe-translated-value value)
                 (enum-alist (gethash (first triple) *mib-enums*)))
	       (unless enum-alist
                 (setf enum-alist (gethash
				   (subseq (first triple) 0 (- (length (first triple)) 1))
				   *mib-enums*)))
	       (when enum-alist
		 (setf maybe-translated-value (cdr (assoc value enum-alist))))
	       (list translated-oid tag maybe-translated-value))
           )
          (t (list translated-oid tag value)))))


(defun snmp-get-many (oid-list)
  "Request  one or more values from the agent, parmater is a list of object
identifiers"
  (let ((triple-list (snmp-get-many- (mapcar #'oid-basic-form oid-list))))
    (loop for triple in triple-list collect (translate-triple triple))))

(defun snmp-get-many-safe- (oid-list identifying-oid in-identifier)
 "This version of snmp-get takes a list of oid's as ususal, but prepends
the list with the oid in the identifying-oid parameter, and thecks the
returned value with in-identifier parameter. The identifying oid can be
the serial number of the agent device. If the serial number is not as
expected, nil is returned. This version of the function does not translate
the result"
  (let ((result+identifier (snmp-get-many- (mapcar #'oid-basic-form (cons identifying-oid oid-list)))))
    (let* ((read-identifier-triple (translate-triple (first result+identifier)))
           (result (rest result+identifier)))
      (when (equal (third read-identifier-triple) in-identifier)
        result))))

(defun snmp-get-many-safe (oid-list identifying-oid in-identifier)
 "This version of snmp-get takes a list of oid's as ususal, but prepends
the list with the oid in the identifying-oid parameter, and thecks the
returned value with in-identifier parameter. The identifying oid can be
the serial number of the agent device. If the serial number is not as
expected, nil is returned"
  (let ((result+identifier (snmp-get-many- (mapcar #'oid-basic-form (cons identifying-oid oid-list)))))
    (let ((read-identifier-triple (translate-triple (first result+identifier)))
          (result (rest result+identifier)))
      (when (equal (third read-identifier-triple) in-identifier)
        (mapcar #'translate-triple result)))))


(defun snmp-get (oid)
  "Returns a single value from the agent
It is presented in its most decoded form,
string-form of oid, string form of octet string, and symbolic
value in case of enumeration
The parameter is an oid in array form, dotted-numeric-form, symbolic form
or a trailing subidentifier"
  (let ((triple (snmp-get- oid)))
    (translate-triple triple)))




;; (defun snmp-get-% (ip community oid)
;;   ""
;;   (let ((response-buffer (udp-send-and-receive 
;;                           ip
;;                           161
;;                           1
;;                           3
;;                           (ber-encode `(:sequence (:integer 0) ; version 1
;;                                         (:octet-string ,community)
;;                                         (:get (:integer 12345)
;;                                          (:integer 0)
;;                                          (:integer 0)
;;                                          (:sequence (:sequence (:object-identifier ,oid) (:null)))))))))
;;     (ber-decode response-buffer 0 (length response-buffer))))


